<?php

/**
 * Plugin Name: Carer Support Referrals by Giant Peach
 * Description: Referral functionality for Carer Support
 * Version: 1.1.3
 * Author: Giant Peach
 * Author URI: https://giantpeach.agency
 * Text Domain: gp_referrals
 * Domain Path: /i18n/languages/
 *
 */

defined('ABSPATH') || exit;

class GP_Referrals
{

    protected static $_instance = null;

    public $version = '1.1.3';

    public function __construct()
    {
        $this->includes();
    }

    public function includes()
    {
        include_once 'includes/class-post-types.php';
        include_once 'includes/models/class-model.php';
        include_once 'includes/models/class-log.php';
        include_once 'includes/class-database.php';
        include_once 'includes/class-requests-clog.php';
        include_once 'includes/class-resources.php';
        include_once 'includes/class-requests.php';
        include_once 'includes/class-fields.php';
        include_once 'includes/class-submissions.php';
        include_once 'includes/class-clog.php';
        include_once 'includes/class-download.php';
        include_once 'includes/class-email.php';
        include_once 'includes/class-api.php';
        include_once 'includes/class-admin.php';
        include_once 'includes/class-validation.php';
    }

    public static function instance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}

/**
 * Returns the main instance.
 *
 * @return GP_Referrals
 */
function gp_referrals()
{ // phpcs:ignore WordPress.NamingConventions.ValidFunctionName.FunctionNameInvalid
    return GP_Referrals::instance();
}

gp_referrals();
