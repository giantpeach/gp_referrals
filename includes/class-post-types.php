<?php

namespace GP_Referrals;

defined('ABSPATH') || exit;

class Post_Types
{

    /**
     * Hook in methods.
     */
    public static function init()
    {
        add_action('init', array(__CLASS__, 'register_post_types'), 5);
        add_action('init', array(__CLASS__, 'register_taxonomies'), 5);
    }

    /**
     * Register core taxonomies.
     */
    public static function register_taxonomies()
    {
        if (taxonomy_exists('gp_referral_form')  && !(apply_filters('gp_referral_register_taxonomies', false))) {
            return;
        }

        register_taxonomy(
            'gp_referral_form',
            'gp_referral',
            apply_filters(
                'gp_referral',
                array(
                    'hierarchical'      => false,
                    'show_ui'           => false,
                    'show_in_nav_menus' => false,
                    'rewrite'           => false,
                    'public'            => false,
                    'show_admin_column' => true,
                    'label' => 'Referral Type'
                )
            )
        );
    }

    /**
     * Register core post types.
     */
    public static function register_post_types()
    {
        if (post_type_exists('gp_referral')) {
            return;
        }

        $labels = apply_filters('gp_referral', array(
            'name'                  => __('Referral Submissions', 'gp_referrals'),
            'singular_name'         => __('Referral Submission', 'gp_referrals'),
            'all_items'             => __('All Referral Submissions', 'gp_referrals'),
            'menu_name'             => _x('Referral Submissions', 'gp_referrals'),
            'add_new'               => __('Add New', 'gp_referrals'),
            'add_new_item'          => __('Add New', 'gp_referrals'),
            'edit'                  => __('Edit', 'gp_referrals'),
            'edit_item'             => __('Edit', 'gp_referrals'),
            'new_item'              => __('New Referral Submission', 'gp_referrals'),
            'view_item'             => __('View Referral Submission', 'gp_referrals'),
            'view_items'            => __('View Referral Submissions', 'gp_referrals'),
            'search_items'          => __('Search by Surname/Email', 'gp_referrals'),
            'not_found'             => __('Nothing found', 'gp_referrals'),
            'not_found_in_trash'    => __('Nothing found in trash', 'gp_referrals')
        ));

        register_post_type(
            'gp_referral',
            apply_filters(
                'gp_referral_post_type_args',
                array(
                    'labels'              => $labels,
                    'public'              => true,
                    'publicly_queryable'  => false,
                    'hierarchical'        => false,
                    'supports'            => array('title'), // title is removed in admin_init hook in class-admin.php
                    'has_archive'         => false,
                    'show_in_rest'        => false,
                    'menu_icon' => 'dashicons-id-alt',
                    'capability_type' => 'post',
                    'capabilities' => array(
                        'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
                    ),
                    'map_meta_cap' => true, // Set to `false`, if users are not allowed to edit/delete existing posts
                )
            )
        );

        do_action('gp_referrals_after_register_post_type');
    }
}

Post_Types::init();
