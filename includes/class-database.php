<?php

namespace GP_Referrals;

defined('ABSPATH') || exit;

class Database
{

    /**
     * DB updates and callbacks that need to be run per plugin version.
     *
     * @var array
     */
    private static $db_updates = array(
        '0.0.1' => array(
            '\gp_referrals_add_entries_table',
        ),
        '0.0.2' => array(
            '\gp_referrals_add_logs_table'
        )
    );

    public static function init()
    {
        include_once dirname(__FILE__) . '/database/update-functions.php';

        add_action('init', array(__CLASS__, 'check_version'), 5);

        // TEST DATA
        // add_action( 'woocommerce_loaded', [__CLASS__, 'create_dummy_exam_sessions'] );
    }

    public static function check_version()
    {
        $gp_referrals_newest_version = \gp_referrals()->version;
        if (version_compare(get_option('gp_referrals_version'), $gp_referrals_newest_version, '<')) {
            self::run_updates();
        }
    }

    public static function get_db_update_callbacks()
    {
        return self::$db_updates;
    }

    public static function run_updates()
    {

        $gp_referrals_newest_version = \gp_referrals()->version;
        $current_db_version = get_option('gp_referrals_version');

        foreach (self::get_db_update_callbacks() as $version => $update_callbacks) {
            $version_updated = false;
            // check this version is ahead of our current version but not ahead of the version we want to update to
            if (version_compare($current_db_version, $version, '<')) {
                if (version_compare($version, $gp_referrals_newest_version, '<=')) {
                    foreach ($update_callbacks as $update_callback) {
                        if (is_callable($update_callback)) {
                            $version_updated = call_user_func($update_callback);
                            if ($version_updated === false) {
                                // there was an error, exit upgrade
                                break 2;
                            }
                        }
                    }
                    if ($version_updated !== false) {
                        update_option('gp_referrals_version', $version);
                    }
                }
            }
        }
    }
}

Database::init();
