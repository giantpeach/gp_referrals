<?php

namespace GP_Referrals;

use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

defined('ABSPATH') || exit;

class Validation
{
    public static function getValidation($data, $type, $request)
    {
        $fields = Fields::all($type);
        $rules = array();
        foreach ($fields as $field) {
            $args = array('message' => sprintf('%s is not valid.', $field['label']));
            if (isset($field['validation_type'])) {
                switch ($field['validation_type']) {
                    case 'string':
                        if (isset($data[$field['name']])) {
                            $args['rule'] = v::optional(v::stringType());
                        }
                        break;
                }
            }
            $rules[$field['name']] = $args;
        }
        // Hardcoded validation
        $rules['email']['rule'] = v::email()::notEmpty();
        $rules['first_name']['rule'] = v::stringType()::notEmpty();
        $rules['surname']['rule'] = v::stringType()::notEmpty();
        if ($data['register'] === 'true') {
            $rules['dob']['rule'] = v::date('Y-m-d')::minAge(16, 'Y-m-d');
            if ($type === 'third-party') {
                $rules['dob']['message'] = 'If you do not know the date of birth of the person you are referring, or if the person you are referring is under the age of 16, please call – 0800 181 4118';
            }
            else {
                $rules['dob']['message'] = 'We cannot accept online registrations from under 16s. If you are under the age of 16, please call 0800 181 4118 to speak to us. <a href="/get-started/under-16s">More information here</a>';
            }
            $rules['address1']['rule'] = v::stringType()::notEmpty();
            $rules['postcode']['rule'] = v::stringType()::notEmpty();
            $rules['gender']['rule'] = v::stringType()::notEmpty();
            $rules['telephone']['rule'] = v::stringType()::notEmpty();
        }
        return apply_filters('gp_referrals_validation_rules', $rules, $data, $type, $request);
    }

    public static function getValidationMessages($validation, $data, $type, $request)
    {
        $messages = array();
        foreach ($validation as $name => $params) {
            if (isset($params['message'])) {
                $messages[$name] = $params['message'];
            }
        }
        return apply_filters('gp_referrals_validation_messages', $messages, $validation, $data, $type, $request);
    }

    /**
     * Validate fields are as expected
     */
    public static function validate_request($data, $type, $request)
    {
        // request_key is just a honeypot to fool spam bots
        if (!isset($data['request_key'])) {
            return new \WP_Error('auth', 'Bad request', ['status' => 400]);
        }

        $validation = self::getValidation($data, $type, $request);
        $messages = self::getValidationMessages($validation, $data, $type, $request);

        try {
            $rules = v::key('email',  v::email());
            foreach ($validation as $name => $params) {
                if (isset($params['rule'])) {
                    $rules->key($name, $params['rule']);
                }
            }
            $rules->assert($data);
        } catch (NestedValidationException $exception) {
            $errors = $exception->getMessages(
                $messages
            );
            return $errors;
        }

        return false;
    }
}

Api::init();
