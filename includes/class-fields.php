<?php

namespace GP_Referrals;

defined('ABSPATH') || exit;

class Fields
{
    public static function json($type)
    {
        $url = apply_filters('gp_referrals_fields_json_url', get_home_url(null, '/app/src/scripts/routes/referral-form-component/json/' . $type . '.json'), $type);
        $request = wp_remote_get($url, array('sslverify' => false));
        $response = wp_remote_retrieve_body($request);
        $json = json_decode($response, true);
        // bit of a hack, but if we are doing new-to-caring, we also need part 2 further-support json
        if ($type == 'new-to-caring') {
            $url = apply_filters('gp_referrals_fields_json_url', get_home_url(null, '/app/src/scripts/routes/referral-form-component/json/further-support.json'), $type);
            $request = wp_remote_get($url, array('sslverify' => false));
            $response = wp_remote_retrieve_body($request);
            $support_json = json_decode($response, true);
            $json = array('pages' => array_merge($json['pages'], $support_json['pages']));
        } else if ($type == 'further-support') {
            $url = apply_filters('gp_referrals_fields_json_url', get_home_url(null, '/app/src/scripts/routes/referral-form-component/json/new-to-caring.json'), $type);
            $request = wp_remote_get($url, array('sslverify' => false));
            $response = wp_remote_retrieve_body($request);
            $new_to_caring_json = json_decode($response, true);
            $json = array('pages' => array_merge($new_to_caring_json['pages'], $json['pages']));
        }
        return $json;
    }

    public static function all($type)
    {
        $json = self::json($type);
        foreach ($json['pages'] as $page) {
            foreach ($page['fieldsets'] as $fieldset) {
                foreach ($fieldset['fields'] as $field) {
                    $fields[$field['name']] = $field;
                }
            }
        }
        return $fields;
    }

    public static function get_field_value($value, $field, $array_format = 'implode')
    {
        if ($field['type'] == 'select' || $field['type'] == 'radio' || $field['type'] == 'radio_list') {
            if (isset($field['options'])) {
                foreach ($field['options'] as $option) {
                    if ("" . $option['value'] === "" . $value) {
                        return $option['label'];
                    }
                }
            }
        }
        if ($field['type'] == 'checkbox') {
            if (is_array($value)) {
                $joined_value = array();
                if (isset($field['options'])) {
                    foreach ($value as $selection) {
                        $found = true;
                        foreach ($field['options'] as $option) {
                            if ("" . $option['value'] === "" . $selection) {
                                $joined_value[] = $option['label'];
                                $found = true;
                            }
                        }
                        if (!$found) {
                            $joined_value[] = $selection;
                        }
                    }
                }
                if ($array_format == 'ul') {
                    return "<ul style='padding-left: 40px; list-style-type: disc;'><li>" . implode("</li><li>", $joined_value) . "</li></ul>";
                }
                return implode(", ", $joined_value);
            } else if (isset($field['options'])) {
                foreach ($field['options'] as $option) {
                    if ("" . $option['value'] === "" . $value) {
                        return $option['label'];
                    }
                }
            }
        }
        if ($value === true) {
            return 'Yes';
        }
        if ($value === false) {
            return 'No';
        }
        // if not fault, default to value
        return $value;
    }

    /**
     * Return the field label for a given field key, given an array of fields
     */
    public static function get_field_label($field_name, $fields)
    {
        if ($field_name == 'dependant_first_name') {
            return 'Dependant First Name';
        }
        if ($field_name == 'dependant_surname') {
            return 'Dependant Surname';
        }
        if ($field_name == 'dependant_dob') {
            return 'Dependant Date of Birth';
        }

        if (isset($fields[$field_name]) && isset($fields[$field_name]['label'])) {
            return strip_tags($fields[$field_name]['label']);
        }
        // might be an array of fields instead of associative array
        foreach ($fields as $field) {
            if ($field['name'] === $field_name && isset($field['label'])) {
                return strip_tags($field['label']);
            }
        }
        return $field_name;
    }
}
