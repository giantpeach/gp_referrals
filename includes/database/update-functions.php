<?php

defined('ABSPATH') || exit;

function gp_referrals_add_entries_table()
{
    global $wpdb;
    $wpdb->query(
        "CREATE TABLE `{$wpdb->prefix}gp_referrals_entries` (
            `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
            `entry_id` BIGINT(20) UNSIGNED NOT NULL,
            `form_id` BIGINT(20) UNSIGNED NOT NULL,
            `field` VARCHAR(255) DEFAULT NULL,
            `value` VARCHAR(255) DEFAULT NULL,
            PRIMARY KEY (`id`)
        )"
    );
}

function gp_referrals_add_logs_table()
{
    global $wpdb;
    return $wpdb->query(
        "CREATE TABLE `{$wpdb->prefix}gp_referrals_logs` (
            `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
            `submission_post_id` BIGINT(20) UNSIGNED NOT NULL,
            `message` LONGTEXT DEFAULT NULL,
            `action` VARCHAR(255) DEFAULT NULL,
            `status` TINYINT NOT NULL DEFAULT 0,
            `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `updated_at` DATETIME DEFAULT NULL,
            PRIMARY KEY (`id`)
        )"
    );
}
// function gp_referrals_entries_indexes() {
//     global $wpdb;
//     return $wpdb->query(
// 		"ALTER TABLE `{$wpdb->prefix}gp_referrals_exam_sessions`
//         ADD KEY `exam_id` (`exam_id`),
//         ADD KEY `user_id` (`user_id`),
//         ADD KEY `status` (`status`);"
// 	);
// }
