<?php

namespace GP_Referrals;

defined('ABSPATH') || exit;

class Download
{

    /**
     * Hook in methods.
     */
    public static function init()
    {
        add_filter('handle_bulk_actions-edit-gp_referral', array(__CLASS__, 'download_csv_bulk_action_handler'), 10, 3);
    }

    public static function download_csv_bulk_action_handler($redirect_to, $doaction, $post_ids)
    {
        if ($doaction !== 'download_csv') {
            return $redirect_to;
        }
        $submissions = array();
        $rows = array();
        $columns = apply_filters('gp_referral_download_csv_columns_order', array('type' => 'Referral', 'date' => 'Submission Date'));
        foreach ($post_ids as $post_id) {
            $submission = Submission::get($post_id);
            if (!empty($submission['data'])) {
                $submissions[] = array_merge(array('type' => $submission['type'], 'date' => $submission['date']), $submission['data']);
            }
        }
        // Prepare list of columns
        foreach ($submissions as $submission) {
            $row = array();
            $fields = Fields::all($submission['type']);
            // Add any new columns
            foreach ($submission as $field_name => $value) {
                if (in_array($field_name, array('hashed_email', 'hashed_surname', 'submission_hash'))) {
                    continue;
                }
                if (!array_key_exists($field_name, $columns)) {
                    $label = Fields::get_field_label($field_name, $fields);
                    $columns[$field_name] = $label;
                }
            }
            // Add row
            foreach ($columns as $key => $column) {
                if (isset($submission[$key])) {
                    if (isset($fields[$key])) {
                        $row[] = Fields::get_field_value($submission[$key], $fields[$key], 'implode');
                    } else {
                        if (is_array($submission[$key])) {
                            $row[] = implode(", ", $submission[$key]);
                        } else {
                            $row[] = $submission[$key];
                        }
                    }
                } else {
                    $row[] = '';
                }
            }
            $rows[] = $row;
        }
        self::download_file($columns, $rows);
    }

    public static function download_file($columns, $rows)
    {
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="export.csv"');
        if (ob_get_contents()) ob_end_clean();

        $output_handle = fopen('php://output', 'w');
        fputcsv($output_handle, $columns);
        foreach ($rows as $row) {
            fputcsv($output_handle, $row);
        }
        fclose($output_handle);
        die();
    }
}

Download::init();
