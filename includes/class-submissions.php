<?php

namespace GP_Referrals;

use DateTime;

defined('ABSPATH') || exit;

class Submission
{
    /**
     * The fields to anonymise if not registering user
     */
    protected static $anonymous_fields = array('title', 'first_name', 'surname', 'dob', 'email', 'address1', 'address2', 'addres3', 'post_town', 'postcode', 'dependant_first_name', 'dependant_surname', 'dependant_dob');

    public static function init()
    {
        add_action('before_delete_post', array(__CLASS__, 'delete'), 10, 2);
    }

    public static function encrypt($value)
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-ctr'));
        $value = openssl_encrypt($value, 'aes-256-ctr', AUTH_KEY, 0, $iv);
        $value = bin2hex($iv) . $value;

        return $value;
    }

    public static function decrypt($value)
    {

        $iv_size = openssl_cipher_iv_length('aes-256-ctr');
        $iv_size = 32;
        $iv = hex2bin(substr($value, 0, $iv_size));
        $value = openssl_decrypt(substr($value, $iv_size), 'aes-256-ctr', AUTH_KEY, 0, $iv);

        return $value;
    }

    public static function save($data, $formType)
    {
        global $wpdb;

        $tableName = $wpdb->prefix . 'gp_referrals_entries';

        // Insert new Post
        $submission_post_id = wp_insert_post([
            'post_type' => 'gp_referral',
            'post_title' => $formType,
            'post_status' => 'publish',
        ]);

        // Tag the submission for the form name
        wp_set_object_terms($submission_post_id, $formType, 'gp_referral_form');

        if (!isset($data['register']) || $data['register'] != 'true') {
            $anonymous_fields = apply_filters('gp_referrals_anonymous_fields', self::$anonymous_fields, $formType, $data);
            foreach ($anonymous_fields as $anonymous_key) {
                if (isset($data[$anonymous_key])) {
                    if ($anonymous_key == 'dob' || $anonymous_key == 'dependant_dob') {
                        // Only keep the year
                        $data[$anonymous_key] = substr($data[$anonymous_key], 0, 4);
                    } else {
                        unset($data[$anonymous_key]);
                    }
                }
            }
        }

        if (isset($data['surname'])) {
            $data['hashed_surname'] = hash('sha256', env('AUTH_KEY') . strtolower($data['surname']));
        }
        if (isset($data['email'])) {
            $data['hashed_email'] = hash('sha256', env('AUTH_KEY') . strtolower($data['email']));
        }

        $data['submission_hash'] = hash('sha256', env('AUTH_KEY') . time());

        if ($submission_post_id !== 0) {
            // Post created successfully

            foreach ($data as $field => $value) {

                if (is_array($value)) {
                    $value = json_encode($value);
                }

                if ($value === true) {
                    $value = 'true';
                }

                if ($value === false) {
                    $value = 'false';
                }

                if ($field != 'hashed_surname' && $field != 'hashed_email') {
                    $value = self::encrypt($value);
                }

                $wpdb->insert($tableName, [
                    'entry_id' => $submission_post_id,
                    'field' => $field,
                    'value' => $value,
                ]);
            }

            Log::create(
                array(
                    'submission_post_id' => $submission_post_id,
                    'status' => Log::STATUS_NOTICE,
                    'action' => 'submission_saved'
                )
            );

            return array(
                'submission_post_id' => $submission_post_id,
                'submission_hash' => $data['submission_hash']
            );
        }

        return false;
    }
    /**
     * Inserts new fields or updates existing fields
     */
    public static function save_fields($submission_post_id, $data)
    {
        global $wpdb;

        $tableName = $wpdb->prefix . 'gp_referrals_entries';

        if (isset($data['surname'])) {
            $data['hashed_surname'] = hash('sha256', env('AUTH_KEY') . $data['surname']);
        }
        if (isset($data['email'])) {
            $data['hashed_email'] = hash('sha256', env('AUTH_KEY') . $data['email']);
        }

        if ($submission_post_id !== 0) {

            foreach ($data as $field => $value) {

                if (is_array($value)) {
                    $value = json_encode($value);
                }

                if ($value === true) {
                    $value = 'true';
                }

                if ($value === false) {
                    $value = 'false';
                }

                if ($field != 'hashed_surname' && $field != 'hashed_email') {
                    $value = self::encrypt($value);
                }

                $update = $wpdb->update($tableName, [
                    'value' => $value,
                    'field' => $field,
                    'entry_id' => (int) $submission_post_id
                ], [
                    'field' => $field,
                    'entry_id' => (int) $submission_post_id,
                ]);
                if ($update === 0) {
                    $wpdb->insert($tableName, [
                        'entry_id' => (int) $submission_post_id,
                        'field' => $field,
                        'value' => $value,
                    ]);
                }
            }

            $formatted_data = str_replace(array("Array", "(", ")"), "", print_r($data, true));

            Log::create(
                array(
                    'submission_post_id' => $submission_post_id,
                    'status' => Log::STATUS_NOTICE,
                    'action' => 'submission_fields_saved',
                    'message' => $formatted_data
                )
            );

            return array(
                'submission_post_id' => $submission_post_id
            );
        }

        return false;
    }

    public static function get($submission_post_id)
    {
        $submission = array('data' => array());
        global $wpdb;
        $tableName = $wpdb->prefix . 'gp_referrals_entries';
        $results = $wpdb->get_results(
            $wpdb->prepare(
                "SELECT `field`, `value` FROM $tableName WHERE entry_id = %d
                AND `field` NOT IN ('hashed_email','hashed_surname')",
                array($submission_post_id)
            ),
            ARRAY_A
        );
        foreach ($results as $result) {
            $submission['data'][$result['field']] = self::decrypt($result['value']);
            if (self::is_field_json($submission['data'][$result['field']])) {
                $submission['data'][$result['field']] = json_decode($submission['data'][$result['field']]);
            }
        }
        if (empty($submission['data'])) {
            return false;
        }

        $terms = get_the_terms($submission_post_id, 'gp_referral_form');
        if (empty($terms)) {
            return false;
        }
        $submission['type'] = $terms[0]->slug;
        $submission['date'] = get_the_date('Y-m-d H:i:s', $submission_post_id);
        $submission['id'] = $submission_post_id;

        return $submission;
    }

    public static function delete($submission_post_id)
    {
        global $wpdb;
        if (get_post_type($submission_post_id) == 'gp_referral') {
            $tableName = $wpdb->prefix . 'gp_referrals_entries';
            $logTableName = $wpdb->prefix . 'gp_referrals_log';
            // Delete fields
            $wpdb->delete($tableName, array('entry_id' => $submission_post_id));
            // Delete logs
            $wpdb->delete($logTableName, array('submission_post_id' => $submission_post_id));
        }
    }

    public static function is_field_json($value)
    {
        if (!empty($value)) {
            $first = $value[0];
            $last = $value[strlen($value) - 1];
            if (($first === '[' && $last === ']') || $first === '{' && $last === '}') {
                json_decode($value);
                return (json_last_error() == JSON_ERROR_NONE);
            }
        }
        return false;
    }

    public static function get_logs($submission_post_id, $statuses_to_show = null, $exam_module_id = null)
    {
        global $wpdb;
        $logs_table_name = Log::get_table_name();

        $sql = "SELECT * FROM `" . $logs_table_name . "` WHERE `submission_post_id`= " . $submission_post_id;

        if ($statuses_to_show) {
            $sql .= " AND `status` IN (" . implode(",", $statuses_to_show) . ")";
        }

        $sql .= " ORDER BY `created_at` ASC";

        $results = $wpdb->get_results(
            $sql,
            ARRAY_A
        );

        if ($results) {
            return $results;
        } else {
            return array();
        }
    }
}

Submission::init();
