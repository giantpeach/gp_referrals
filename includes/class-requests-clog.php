<?php

namespace GP_Referrals;

use DateTime;

defined('ABSPATH') || exit;

class ClogRequests
{
    public static function get($type)
    {
        switch ($type) {
            case 'new-to-caring':
            case 'further-support':
                return self::get_new_to_caring_requests();
                break;
            case 'third-party':
                return self::get_third_party_requests();
                break;
            case 'self-registration':
                return self::get_self_registration_requests();
                break;
            default:
                return array();
        }
    }

    public static function get_person_details()
    {
        $details = array(
            'first_name' => '{first_name}',
            'surname' => '{surname}',
            'gender' => '{gender}',
            'title' => '{title}',
            'birth_year' => '{dob.year}',
            'birth_month' => '{dob.month}',
            'birth_day' => '{dob.day}',
            'address1' => '{address1}',
            'address2' => '{address2}',
            'address3' => '{address3}',
            'post_town' => '{post_town}',
            'county' => '{post_county}',
            'postcode' => '{postcode}',
            'telephone' => '{telephone}',
            'telephone2' => '{telephone2}',
            'email' => '{email}',
            'wants_to_recieve_the_newsletter' => array(
                'value' => 1475,
                'includeIf' =>
                array(
                    'newsletter_opt_in' => true,
                ),
            ),
            'delivery_method_of_bi_annual_newsletter' => array(
                'value' => 1477,
                'includeIf' =>
                array(
                    'newsletter_opt_in' => true,
                ),
            ),
            'would_the_carer_like_monthly_event_listings' => array(
                'value' => 1479,
                'includeIf' =>
                array(
                    'events_opt_in' => true,
                ),
            )
        );
        return apply_filters('gp_referral_clog_personal_details_fields', $details);
    }

    public static function get_self_registration_requests()
    {
        $requests = array(
            array(
                'name' => 'create_client',
                'method' => 'PUT',
                'endpoint' => '/v2/clients',
                'with_response' => array(
                    'client' => 'id'
                ),
                'fields' => array_merge(
                    self::get_person_details(),
                    array(
                        'consent' => 'Y'
                    )
                ),
                'runIf' =>
                array(
                    'new_client' => true,
                ),
            ),
            array(
                'name' => 'update_client',
                'method' => 'POST',
                'endpoint' => '/v2/clients/id/{client}',
                'fields' => array_merge(
                    self::get_person_details(),
                    array()
                ),
                'runIf' =>
                array(
                    'new_client' => false,
                ),
            ),
            array(
                'name' => 'new_client_registration_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'registration_referral' => 'id'
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 96,
                    'template' => 26,
                    'stage' => 325,
                    'referrer' => 160,
                    'user' => 225,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Self-registration. If support needed Triage project has been opened.

                    Outline of Caring situation: {situation}',
                ),
                'runIf' =>
                array(
                    'new_client' => true,
                ),
            ),
            array(
                'name' => 'new_client_registration_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{registration_referral}',
                'fields' =>
                array(
                    'stage' => 325,
                    'user' => 43,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Send registration information to carer including privacy policy',
                ),
                'runIf' =>
                array(
                    'new_client' => true,
                ),
            ),
            array(
                'name' => 'existing_client_registration_re_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'registration_referral' => 'id'
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 99,
                    'template' => 24,
                    'stage' => 326,
                    'referrer' => 160,
                    'user' => 225,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Self-registration. If support needed Triage project has been opened.

                    Outline of Caring situation: {situation}',
                ),
                'runIf' =>
                array(
                    'new_client' => false,
                ),
            ),
            array(
                'name' => 'existing_client_registration_re_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{registration_referral}',
                'fields' =>
                array(
                    'stage' => 231,
                    'user' => 43,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Check if Triage project open, if not send carer a letter informing them we have had a referral and contact us if they need support.',
                ),
                'runIf' =>
                array(
                    'new_client' => false,
                ),
            ),
            array(
                'name' => 'triage_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'triage_referral' => 'id'
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 98,
                    'template' => 28,
                    'stage' => 231,
                    'referrer' => 160,
                    'user' => 225,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Outline of Caring situation: {situation}',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'client_under_18' => false
                ),
            ),
            array(
                'name' => 'triage_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{triage_referral}',
                'fields' =>
                array(
                    'stage' => 231,
                    'user' => 45,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Carer would like a support conversation with a community connector. Contact carer to provide support. Looking for: {support_looking_for}',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'client_under_18' => false
                ),
            ),
            array(
                'name' => 'transition_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'transition_referral' => 'id'
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 75,
                    'template' => 104,
                    'stage' => 558,
                    'referrer' => 160,
                    'user' => 225,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Outline of Caring situation: {situation}',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'client_under_18' => true
                ),
            ),
            array(
                'name' => 'transition_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{transition_referral}',
                'fields' =>
                array(
                    'stage' => 492,
                    'user' => 204,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Contact carer to provide support/undertake transition assessment.',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'client_under_18' => true
                ),
            )
        );
        return apply_filters('gp_referrals_clog_requests_self_registration', $requests);
    }

    public static function get_third_party_requests()
    {
        $requests = array(
            array(
                'name' => 'create_client',
                'method' => 'PUT',
                'endpoint' => '/v2/clients',
                'with_response' => array(
                    'client' => 'id'
                ),
                'fields' => array_merge(
                    self::get_person_details(),
                    array(
                        'consent' => 'Y'
                    )
                ),
                'runIf' =>
                array(
                    'new_client' => true,
                ),
            ),
            array(
                'name' => 'update_client',
                'method' => 'POST',
                'endpoint' => '/v2/clients/id/{client}',
                'fields' => array_merge(
                    self::get_person_details(),
                    array()
                ),
                'runIf' =>
                array(
                    'new_client' => false,
                ),
            ),
            array(
                'name' => 'new_client_registration_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'registration_referral' => 'id'
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 96,
                    'template' => 27,
                    'stage' => 327,
                    'referrer' => 296,
                    'user' => 225,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Referral from third party - details of referrer in extension database. If support needed Triage project has been opened.

                    Outline of Caring situation: {situation}',
                    'organisation_referring' => '{organisation_referring}',
                    'referrer_contact_number_1' => '{referrer_contact_number_1}',
                ),
                'runIf' =>
                array(
                    'new_client' => true,
                ),
            ),
            array(
                'name' => 'new_client_registration_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{registration_referral}',
                'fields' =>
                array(
                    'stage' => 325,
                    'user' => 43,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Send registration information to carer including privacy policy',
                ),
                'runIf' =>
                array(
                    'new_client' => true,
                ),
            ),
            array(
                'name' => 'existing_client_registration_re_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'registration_referral' => 'id'
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 97,
                    'template' => 25,
                    'stage' => 328,
                    'referrer' => 296,
                    'user' => 225,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Website Referral from third party - details of referrer in extension database. If support needed Triage project has been opened.

                    Outline of Caring situation: {situation}',
                    'organisation_referring' => '{organisation_referring}',
                    'referrer_contact_number_1' => '{referrer_contact_number_1}',
                ),
                'runIf' =>
                array(
                    'new_client' => false,
                ),
            ),
            array(
                'name' => 'existing_client_registration_re_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{registration_referral}',
                'fields' =>
                array(
                    'stage' => 328,
                    'user' => 43,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Check if Triage project open, if not send carer a letter informing them we have had a referral and contact us if they need support.',
                ),
                'runIf' =>
                array(
                    'new_client' => false,
                ),
            ),
            array(
                'name' => 'triage_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'triage_referral' => 'id'
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 98,
                    'template' => 28,
                    'stage' => 231,
                    'referrer' => 296,
                    'user' => 225,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Outline of Caring situation: {situation}',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'client_under_18' => false
                ),
            ),
            array(
                'name' => 'triage_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{triage_referral}',
                'fields' =>
                array(
                    'stage' => 231,
                    'user' => 45,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Carer would like a support conversation with a community connector. Contact carer to provide support. Looking for: {support_looking_for}',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'client_under_18' => false
                ),
            ),
            array(
                'name' => 'transition_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'transition_referral' => 'id'
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 75,
                    'template' => 104,
                    'stage' => 558,
                    'user' => 225,
                    'referrer' => 296,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Outline of Caring situation: {situation}',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'client_under_18' => true
                ),
            ),
            array(
                'name' => 'transition_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{transition_referral}',
                'fields' =>
                array(
                    'stage' => 492,
                    'user' => 204,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Contact carer to provide support/undertake transition assessment.',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'client_under_18' => true
                ),
            )
        );
        return apply_filters('gp_referrals_clog_requests_third_party', $requests);
    }

    public static function get_new_to_caring_requests()
    {
        $extension_database_fields = apply_filters('gp_referral_clog_extension_database_fields', array(
            'if_you_were_suddenly_taken_ill_would_name_of' => '{taken_ill_be_ok}',
            'are_you_able_to_leave_name_of_relative_parent_' => '{are_you_able_to_leave_alone}',
            'how_would_you_describe_your_financial_situation_' => '{how_would_you_describe_your_financial_situation_}',
            'does_your_caring_role_still_allow_you_time_for' => '{does_your_caring_role_still_allow_you_time_for}',
            'how_do_you_feel_about_your_caring_role_' => '{how_do_you_feel_about_your_caring_role_}',
            'other_please_give_information_' => '{why_questionnaire_other}',
            'other_please_specify_' => '{caring_responsibilities_other}',
            'other_please_specify_1' => '{help_manage_role_other}',
            // Why have you decided to take
            'my_caring_situation_is_becoming_more_complex' => array(
                'value' => '1602',
                'includeIf' =>
                array(
                    'why_questionnaire' => 'my_caring_situation_is_becoming_more_complex',
                ),
            ),
            '_relative_first_name_parent_type_ie_mum_dad_is' => array(
                'value' => '1604',
                'includeIf' =>
                array(
                    'why_questionnaire' => '_relative_first_name_parent_type_ie_mum_dad_is',
                ),
            ),
            'i_think_relative_name_parent_is_reaching_the' => array(
                'value' => '1606',
                'includeIf' =>
                array(
                    'why_questionnaire' => 'i_think_relative_name_parent_is_reaching_the',
                ),
            ),
            'i_rsquo_m_giving_care_already_but_i_need_support' => array(
                'value' => '1608',
                'includeIf' =>
                array(
                    'why_questionnaire' => 'i_rsquo_m_giving_care_already_but_i_need_support',
                ),
            ),
            'i_rsquo_m_just_thinking_ahead' => array(
                'value' => '1610',
                'includeIf' =>
                array(
                    'why_questionnaire' => 'i_rsquo_m_just_thinking_ahead',
                ),
            ),
            // What are your caring responsibilities
            'personal_care_such_as_washing_dressing' => array(
                'value' => '1612',
                'includeIf' =>
                array(
                    'caring_responsibilities' => 'personal_care_such_as_washing_dressing',
                ),
            ),
            'help_with_moving_around' => array(
                'value' => '1614',
                'includeIf' =>
                array(
                    'caring_responsibilities' => 'help_with_moving_around',
                ),
            ),
            'practical_help_such_as_cooking_meals_doing_the' => array(
                'value' => '1616',
                'includeIf' =>
                array(
                    'caring_responsibilities' => 'practical_help_such_as_cooking_meals_doing_the',
                ),
            ),
            'arranging_medical_appointments_amp_care_services' => array(
                'value' => '1618',
                'includeIf' =>
                array(
                    'caring_responsibilities' => 'arranging_medical_appointments_amp_care_services',
                ),
            ),
            'giving_medicines' => array(
                'value' => '1620',
                'includeIf' =>
                array(
                    'caring_responsibilities' => 'giving_medicines',
                ),
            ),
            'helping_with_paperwork' => array(
                'value' => '1622',
                'includeIf' =>
                array(
                    'caring_responsibilities' => 'helping_with_paperwork',
                ),
            ),
            'keeping_an_eye_on_them_to_make_sure_they_are_safe' => array(
                'value' => '1624',
                'includeIf' =>
                array(
                    'caring_responsibilities' => 'keeping_an_eye_on_them_to_make_sure_they_are_safe',
                ),
            ),
            'providing_emotional_support' => array(
                'value' => '1626',
                'includeIf' =>
                array(
                    'caring_responsibilities' => 'providing_emotional_support',
                ),
            ),
            'advocating_for_them_speaking_on_their_behalf_' => array(
                'value' => '1628',
                'includeIf' =>
                array(
                    'caring_responsibilities' => 'advocating_for_them_speaking_on_their_behalf_',
                ),
            ),
            // What would help you with caring?
            'regular_breaks_from_my_caring_role' => array(
                'value' => '1650',
                'includeIf' =>
                array(
                    'help_manage_role' => 'regular_breaks_from_my_caring_role',
                ),
            ),
            'attending_a_carer_caf_eacute_or_group' => array(
                'value' => '1652',
                'includeIf' =>
                array(
                    'help_manage_role' => 'attending_a_carer_caf_eacute_or_group',
                ),
            ),
            'i_would_like_training_in_a_specific_area_e_g_' => array(
                'value' => '1654',
                'includeIf' =>
                array(
                    'help_manage_role' => 'i_would_like_training_in_a_specific_area_e_g_',
                ),
            ),
            'being_able_to_talk_about_my_caring_role' => array(
                'value' => '1656',
                'includeIf' =>
                array(
                    'help_manage_role' => 'being_able_to_talk_about_my_caring_role',
                ),
            ),
            'being_able_to_do_something_for_me_ie_a_pamper' => array(
                'value' => '1658',
                'includeIf' =>
                array(
                    'help_manage_role' => 'being_able_to_do_something_for_me_ie_a_pamper',
                ),
            )
        ));

        $requests = array(
            array(
                'name' => 'create_client',
                'method' => 'PUT',
                'endpoint' => '/v2/clients',
                'with_response' => array(
                    'client' => 'id'
                ),
                'fields' => array_merge(
                    self::get_person_details(),
                    array(
                        'employment_status' => '{work_status}',
                        'consent' => 'Y'
                    )
                ),
                'runIf' =>
                array(
                    'new_client' => true,
                ),
            ),
            array(
                'name' => 'update_client',
                'method' => 'POST',
                'endpoint' => '/v2/clients/id/{client}',
                'fields' => array_merge(
                    self::get_person_details(),
                    array('employment_status' => '{work_status}')
                ),
                'runIf' =>
                array(
                    'new_client' => false,
                ),
            ),
            array(
                'name' => 'create_registration_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'registration_referral' => 'id',
                ),
                'fields' => array_merge(
                    array(
                        'client' => '{client}',
                        'client_type' => 1,
                        'project' => 96,
                        'template' => 26,
                        'stage' => 325,
                        'user' => 225,
                        'referrer' => 160,
                        'date' => '{date.Y-m-d}',
                        'response_description' => '[Website] Completed New to Caring form on website. Details in extension database.',
                    ),
                    $extension_database_fields
                ),
                'runIf' =>
                array(
                    'new_client' => true,
                    'referral_sent' => false
                ),
            ),
            array(
                'name' => 'create_registration_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{registration_referral}',
                'with_response' => array(
                    'referral_sent' => true
                ),
                'fields' =>
                array(
                    'stage' => 231,
                    'user' => 43,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Send carer registration information including the privacy policy.',
                ),
                'runIf' =>
                array(
                    'new_client' => true,
                    'referral_sent' => false
                ),
            ),
            array(
                'name' => 'create_re_registration_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'registration_referral' => 'id',
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 99,
                    'template' => 24,
                    'stage' => 326,
                    'user' => 225,
                    'referrer' => 160,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Completed New to Caring form on website. Details in extension database.'
                ),
                'fields' => array_merge(
                    array(
                        'client' => '{client}',
                        'client_type' => 1,
                        'project' => 99,
                        'template' => 24,
                        'stage' => 326,
                        'user' => 225,
                        'referrer' => 160,
                        'date' => '{date.Y-m-d}',
                        'response_description' => '[Website] Completed New to Caring form on website. Details in extension database.'
                    ),
                    $extension_database_fields
                ),
                'runIf' =>
                array(
                    'new_client' => false,
                    'referral_sent' => false
                ),
            ),
            array(
                'name' => 'create_re_registration_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{registration_referral}',
                'with_response' => array(
                    'referral_sent' => true
                ),
                'fields' =>
                array(
                    'stage' => 231,
                    'user' => 43,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Self referral for existing carer recieved. Check to see if Triage project open, if not send Carer letter saying their referral has been recieved and if they need support contact CSW',
                ),
                'runIf' =>
                array(
                    'new_client' => false,
                    'referral_sent' => false
                ),
            ),
            array(
                'name' => 'create_dependant',
                'method' => 'PUT',
                'endpoint' => '/v2/cared_fors',
                'with_response' => array(
                    'dependant' => 'id'
                ),
                'fields' =>
                array(
                    'first_name' => '{dependant_first_name}',
                    'surname' => '{dependant_surname}',
                    'main_disability' => '{main_disability}',
                    'birth_year' => array(
                        'value' => '{dependant_dob.year}',
                        'includeIf' =>
                        array(
                            'dependant_dob' => true,
                        ),
                    ),
                    'birth_month' => array(
                        'value' => '{dependant_dob.month}',
                        'includeIf' =>
                        array(
                            'dependant_dob' => true,
                        ),
                    ),
                    'birth_day' => array(
                        'value' => '{dependant_dob.day}',
                        'includeIf' =>
                        array(
                            'dependant_dob' => true,
                        ),
                    ),
                    'age_range' => array(
                        'value' => '{dependant_age_range}',
                        'includeIf' =>
                        array(
                            'dependant_age_range' => true,
                        ),
                    ),
                ),
                'runIf' =>
                array(
                    'client' => true,
                    'new_dependant' => true
                ),
            ),
            array(
                'name' => 'update_dependant',
                'method' => 'POST',
                'endpoint' => '/v2/clients/id/{dependant}',
                'fields' =>
                array(
                    'first_name' => '{dependant_first_name}',
                    'surname' => '{dependant_surname}',
                    'main_disability' => '{main_disability}',
                    'birth_year' => array(
                        'value' => '{dependant_dob.year}',
                        'includeIf' =>
                        array(
                            'dependant_dob' => true,
                        ),
                    ),
                    'birth_month' => array(
                        'value' => '{dependant_dob.month}',
                        'includeIf' =>
                        array(
                            'dependant_dob' => true,
                        ),
                    ),
                    'birth_day' => array(
                        'value' => '{dependant_dob.day}',
                        'includeIf' =>
                        array(
                            'dependant_dob' => true,
                        ),
                    ),
                    'age_range' => array(
                        'value' => '{dependant_age_range}',
                        'includeIf' =>
                        array(
                            'dependant_age_range' => true,
                        ),
                    ),
                ),
                'runIf' =>
                array(
                    'dependant' => true,
                    'new_dependant' => false
                ),
            ),
            array(
                'name' => 'create_dependant_relationship',
                'method' => 'PUT',
                'endpoint' => '/v2/relationship_details/{client}/{dependant}',
                'fields' =>
                array(
                    'carer_relation_first_to_second' => '1',
                    'carer_relation_second_to_first' => '3',
                    'relation_first_to_second' => '{relationship.0}',
                    'relation_second_to_first' => '{relationship.1}',
                    'major' => 'Y',
                ),
                'raw' => true,
                'runIf' =>
                array(
                    'dependant' => true,
                ),
            ),
            array(
                'name' => 'triage_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'triage_referral' => 'id'
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 98,
                    'template' => 28,
                    'stage' => 231,
                    'user' => 225,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Carer would like a support conversation with a community connector.',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'triage_referral_sent' => false,
                    'client_under_18' => false
                ),
            ),
            array(
                'name' => 'triage_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{triage_referral}',
                'with_response' => array(
                    'triage_referral_sent' => true
                ),
                'fields' =>
                array(
                    'stage' => 231,
                    'user' => 45,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Carer would like a support conversation with a community connector. Contact carer to provide support.',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'triage_referral_sent' => false,
                    'client_under_18' => false
                ),
            ),
            array(
                'name' => 'transition_referral',
                'method' => 'PUT',
                'endpoint' => '/v2/referrals/',
                'with_response' => array(
                    'transition_referral' => 'id'
                ),
                'fields' =>
                array(
                    'client' => '{client}',
                    'client_type' => 1,
                    'project' => 75,
                    'template' => 104,
                    'stage' => 558,
                    'referrer' => 160,
                    'user' => 225,
                    'date' => '{date.Y-m-d}',
                    'response_description' => '[Website] Help with caring situation: {help_manage_role}. See registration referral for more information (Website Survey).',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'triage_referral_sent' => false,
                    'client_under_18' => true
                ),
            ),
            array(
                'name' => 'transition_referral_due_contact',
                'method' => 'PUT',
                'endpoint' => '/v2/due_contacts/referral/{transition_referral}',
                'with_response' => array(
                    'triage_referral_sent' => true
                ),
                'fields' =>
                array(
                    'stage' => 492,
                    'user' => 204,
                    'start_date' => '{date.Y-m-d H:i:s}',
                    'end_date' => '{date.Y-m-d H:i:s}',
                    'description' => '[Website] Contact carer to provide support/undertake transition assessment.',
                ),
                'runIf' =>
                array(
                    'support_required' => true,
                    'triage_referral_sent' => false,
                    'client_under_18' => true
                ),
            )
        );
        return apply_filters('gp_referrals_clog_requests_new_to_caring', $requests);
    }
}
