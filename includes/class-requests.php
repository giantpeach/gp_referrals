<?php

namespace GP_Referrals;

use DateTime;

defined('ABSPATH') || exit;

class Requests
{
    /**
     * Prepare the request method, endpoint and body for sending to CLOG
     */
    public static function prepare_request($data, $api_request, $submission)
    {
        $api_data = array();
        $json_fields = Fields::all($submission['type']);
        $endpoint = self::prepare_field($api_request['endpoint'], $data, $submission, $json_fields);
        foreach ($api_request['fields'] as $field_name => $field) {
            if (is_array($field)) { // conditional field
                if (self::should_include_field($field, $data)) {
                    $field_value = self::prepare_field($field['value'], $data, $submission, $json_fields);
                    if ($field_value) {
                        $api_data[$field_name] = $field_value;
                    }
                }
            } else {
                $field_value = self::prepare_field($field, $data, $submission, $json_fields);
                if ($field_value) {
                    $api_data[$field_name] = $field_value;
                }
            }
        }
        $run = array(
            'endpoint' => $endpoint,
            'data' => $api_data,
            'method' => $api_request['method'],
            'headers' => array()
        );
        // relationship request for some reason expects raw json body instead of every other request... so we have to do this
        // if raw == true
        if (isset($api_request['raw']) && $api_request['raw'] === true) {
            $run['data'] = json_encode($api_data);
            $run['headers'] = array('Content-Type' => 'application/json');
        }
        return $run;
    }

    /**
     * Please forgive me for this disgusting function.
     * Basically if the field is "{fieldname}" we need to replace with $data['fieldname']
     * But there are some exceptions if it is "{dob.year}" or "{dependand_dob.month}" or "{date.Y-m-d H:i:s}"
     * Or relationship fields which are {relationship.0} (eg.4) or {relationship.1} (eg.5) from example "4;5"
     */
    public static function prepare_field($field_value, $data, $submission, $json_fields)
    {
        preg_match_all('/{(.*?)}/', $field_value, $matches);
        // if we
        if (!empty($matches[1])) {
            foreach ($matches[1] as $match) {
                // Check if variable is some kind of custom property with . notation
                if ($match === 'relationship') {
                    $match_arr = explode(".", $match);
                }
                $match_arr = explode(".", $match);
                if (count($match_arr) > 1) {
                    if (isset($data[$match_arr[0]]) && in_array($match_arr[1], array('year', 'month', 'day'))) {
                        // Should be a date variable
                        $date = date_create_from_format('Y-m-d', $data[$match_arr[0]]);
                        if ($date && $date->format('Y-m-d') === $data[$match_arr[0]]) {
                            if ($match_arr[1] === 'year') {
                                $field_value = str_replace('{' . $match . '}', $date->format('Y'), $field_value);
                            }
                            if ($match_arr[1] === 'month') {
                                $field_value = str_replace('{' . $match . '}', $date->format('n'), $field_value);
                            }
                            if ($match_arr[1] === 'day') {
                                $field_value = str_replace('{' . $match . '}', $date->format('j'), $field_value);
                            }
                        }
                    } else if ($match_arr[0] === 'date') {
                        // Should be the submission date variable
                        if (isset($submission['date'])) {
                            $date = date_create_from_format('Y-m-d H:i:s', $submission['date']);
                        } else {
                            $date = new DateTime();
                        }
                        $field_value = str_replace('{' . $match . '}', $date->format($match_arr[1]), $field_value);
                    } else if ($match_arr[0] === 'relationship' && isset($data['relationship'])) {
                        /* Should be a relationship variable
                        Relationship value is stored like "4;5"
                        ...meanning relationship first to second is 4
                        ...and second to first is 5
                        We need to explode and get the values for each
                        */
                        if (isset($submission['date'])) {
                            $relationship = explode(";", $data['relationship']);
                            if ($match_arr[1] === '0') {
                                $field_value = str_replace('{' . $match . '}', $relationship[0], $field_value);
                            } else if ($match_arr[1] === '1') {
                                $field_value = str_replace('{' . $match . '}', $relationship[1], $field_value);
                            }
                        } else {
                            $date = new DateTime();
                        }
                    }
                } else {
                    if (isset($data[$match])) {
                        // if checkbox multi-select
                        if (is_array($data[$match])) {
                            $field_value = str_replace('{' . $match . '}', Fields::get_field_value($data[$match], $json_fields[$match]), $field_value);
                        }
                        // if other input
                        else {
                            $field_value = str_replace('{' . $match . '}', $data[$match], $field_value);
                        }
                    } else if ($field_value === '{' . $match . '}') {
                        $field_value = '';
                    }
                }
            }
        }
        return $field_value;
    }

    public static function handle_response($data, $response, $api_request)
    {
        if (isset($api_request['with_response'])) {
            foreach ($api_request['with_response'] as $response_field_name => $response_body_field) {
                if (isset($response['body'][$response_body_field])) {
                    $data[$response_field_name] = isset($response['body'][$response_body_field]) ? $response['body'][$response_body_field] : false;
                } else {
                    $data[$response_field_name] = $response_body_field;
                }
            }
        }
        return $data;
    }

    public static function should_run($data, $api_request)
    {
        if (!isset($api_request['runIf'])) {
            return true;
        }
        $invalid = false;
        $match = false;
        foreach ($api_request['runIf'] as $key => $runIf) {
            if (isset($data[$key])) {
                $check = $data[$key];
                if (is_array($check) && in_array($runIf, $check)) {
                    $match = true;
                } else if ($check == $runIf) {
                    $match = true;
                } else {
                    $invalid = true;
                    break;
                }
            }
        }
        return ($match && !$invalid);
    }

    public static function should_include_field($field, $data)
    {
        if (!isset($field['includeIf'])) {
            return true;
        }
        $invalid = false;
        $match = false;
        foreach ($field['includeIf'] as $key => $includeIf) {
            if (isset($data[$key])) {
                $check = $data[$key];
                if (is_array($check) && in_array($includeIf, $check)) {
                    $match = true;
                } else if ($check == $includeIf) {
                    $match = true;
                } else {
                    $invalid = true;
                    break;
                }
            }
        }
        return ($match && !$invalid);
    }
}
