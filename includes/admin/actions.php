<?php

namespace GP_Referrals;

defined( 'ABSPATH' ) || exit;

class Actions {

    public static function verify_nonce($key) {

        // In our file that handles the request, verify the nonce.
        $nonce = esc_attr( $_REQUEST['_wpnonce'] );

        if ( ! wp_verify_nonce( $nonce, $key ) ) {
            die( 'Expired or invalid request' );
        }
    }

    public static function get_action() {
        if ( isset( $_REQUEST['filter_action'] ) && ! empty( $_REQUEST['filter_action'] ) ) {
			return false;
		}

		if ( isset( $_REQUEST['action'] ) && -1 != $_REQUEST['action'] ) {
			return $_REQUEST['action'];
		}

		if ( isset( $_REQUEST['action2'] ) && -1 != $_REQUEST['action2'] ) {
			return $_REQUEST['action2'];
        }
    }

    public static function get_redirect() {

        $redirect = '';

		if ( isset( $_REQUEST['redirect'] ) && -1 != $_REQUEST['redirect'] ) {
			$redirect = $_REQUEST['redirect'];
        }

        if (empty($redirect)) {
            $redirect = wp_get_referer();
        }

        return $redirect;
    }

    public static function process_actions() {
        // In our file that handles the request, verify the nonce.
        $action = self::get_action();
        // if it's a bulk action, these use a different nonce
        if (!$action || substr($action, 0, 5) == "bulk-") return;
        self::verify_nonce($action);
        $redirect = self::get_redirect();

        if ($action == 'reset_exam_module_session') {
            self::reset_exam_module_session($_REQUEST['exam_module_session_id']);
        }
        else if ($action == 'save_exam_session') {
            self::save_exam_session($_REQUEST['exam_session_id']);
        }
        else {
            // action not found
        }

        wp_redirect( $redirect );
        exit;
    }

    /*
    public static function bulk_reset_exam_module_sessions() {

        $ids = esc_sql( $_POST['bulk-reset'] );

        // loop over the array of record IDs and delete them
        foreach ( $ids as $id ) {
            self::reset_exam_module_session($id);
        }

    }
    */
    /*
    public static function get_delete_exam_session_url($exam_session_id, $redirect = null) {
        return sprintf( '?page=%s&action=%s&exam_session_id=%s&_wpnonce=%s&redirect=%s', 'exam_grading', 'delete_exam_session', absint( $exam_session_id ), wp_create_nonce('delete_exam_session'), $redirect );
    }

    public static function get_view_exam_session_url($exam_session_id, $redirect = null) {
        return sprintf( '?page=%s&view_session_id=%s', 'exam_grading', absint( $exam_session_id ) );
    }

    public static function get_reset_exam_session_url($exam_session_id, $redirect = null) {
        return sprintf( '?page=%s&action=%s&exam_session_id=%s&_wpnonce=%s&redirect=%s', 'exam_grading', 'reset_exam_session', absint( $exam_session_id ), wp_create_nonce('reset_exam_session'), $redirect );
    }

    public static function get_save_exam_session_url($exam_session_id, $redirect = null) {
        return sprintf( '?page=%s&action=%s&exam_session_id=%s&_wpnonce=%s&redirect=%s', 'exam_grading', 'save_exam_session', absint( $exam_session_id ), wp_create_nonce('save_exam_session'), $redirect );
    }

    public static function get_reset_exam_module_session_url($exam_module_session_id, $redirect = null) {
        return sprintf( '?page=%s&action=%s&exam_module_session_id=%s&_wpnonce=%s&redirect=%s', 'exam_grading', 'reset_exam_module_session', absint( $exam_module_session_id ), wp_create_nonce('reset_exam_module_session'), $redirect );
    }

    public static function get_start_exam_grading_url($exam_session_id, $redirect = null) {
        return sprintf( '?page=%s&action=%s&exam_session_id=%s&_wpnonce=%s&redirect=%s', 'exam_grading', 'start_exam_grading', absint( $exam_session_id ), wp_create_nonce('start_exam_grading'), $redirect );
    }

    public static function get_reset_exam_grading_url($exam_grading_id, $redirect = null) {
        return sprintf( '?page=%s&action=%s&exam_grading_id=%s&_wpnonce=%s&redirect=%s', 'exam_grading', 'reset_exam_grading', absint( $exam_grading_id ), wp_create_nonce('reset_exam_grading'), $redirect );
    }

    public static function get_resolve_exam_error_url($error_id, $redirect = null) {
        return sprintf( '?page=%s&action=%s&error_id=%s&_wpnonce=%s&redirect=%s', 'exam_grading', 'resolve_exam_error', absint( $error_id ), wp_create_nonce('resolve_exam_error'), $redirect );
    }
*/
}

Grading::init();
