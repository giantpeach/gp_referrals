<?php

namespace GP_Referrals;

defined('ABSPATH') || exit;

class Api
{

    public static function init()
    {
        add_action('rest_api_init', array(__CLASS__, 'register_routes'), 5);
        add_action('wp_head', array(__CLASS__, 'rest_api_nonce'), 5);
        add_action('gp_referral_thank_you', array(__CLASS__, 'handle_clog_requests_from_thank_you'), 5, 2);
        add_action('send_to_clog', array(__CLASS__, 'handle_clog_requests_from_admin'), 5);
    }

    public static function rest_api_nonce()
    {
        $nonce = wp_create_nonce('wp_rest');
        $action_nonce = wp_create_nonce('gp_referrals_action_nonce_referring');
?>
        <script>
            window.gp_referrals_nonce = '<?php echo $nonce; ?>';
            window.gp_referrals_action_nonce = '<?php echo $action_nonce; ?>';
        </script>
<?php
    }

    public static function register_routes()
    {

        register_rest_route(
            'referrals',
            '/submit/(?P<form_type>\S+)',
            array(
                'methods' => 'POST',
                'callback' => array(__CLASS__, 'save_submission')
            )
        );

        register_rest_route(
            'referrals',
            '/dictionary',
            array(
                'methods' => 'GET',
                'callback' => array(__CLASS__, 'test_api_dictionary')
            )
        );
    }

    /**
     * Convert data into the formats we want.
     */
    public static function handle_data($data, $type)
    {
        $fields = Fields::all($type);
        foreach ($data as $field => $value) {
            if ($field === $value) {
                $data[$field] = 'true';
            }
            // convert date to Y-m-d
            if (isset($fields[$field]) && $fields[$field]['type'] === 'date') {
                $date = new \DateTime($value);
                if ($date && $date->format('Y-m-d')) {
                    $data[$field] = $date->format('Y-m-d');
                } else {
                    $data[$field] = $value;
                }
            }
            if ($value === true) {
                // prevent submission value being saved as 1, "true" is preferred for later
                $data[$field] = 'true';
            } elseif ($value === false) {
                $data[$field] = 'false';
            }
            if (is_array($value)) {
                foreach ($data[$field] as $key => $array_item) {
                    $data[$field][$key] = filter_var($array_item, FILTER_SANITIZE_STRING);
                }
            } else {
                $data[$field] = filter_var($data[$field], FILTER_SANITIZE_STRING);
            }
        }
        if (isset($data['register']) && $data['register'] !== 'true') {
            // If user did not opt in to registration, but later requested support, set registration opt-in
            if (isset($data['support_required']) && $data['support_required'] === 'true') {
                $data['register'] = 'true';
            }
        }
        if ($type == 'self-registration' || $type == 'third-party') {
            $data['register'] = 'true';
        }
        $data = apply_filters('gp_referral_submission_handle_data', $data);
        return $data;
    }

    public static function convert_to_boolean($fieldname, $data)
    {
        if (isset($data[$fieldname])) {
            if ($data[$fieldname] === "0" || $data[$fieldname] === "false") {
                return false;
            } else {
                return (bool) $data[$fieldname];
            }
        }
        return false;
    }

    public static function handle_api_errors($response)
    {
        // great function
        return false;
    }

    /**
     * Send search request to CLOG, searching by surname and DOB
     */
    public static function search_client($data)
    {
        if (isset($data['dob']) && isset($data['surname'])) {
            $surname = sanitize_text_field($data['surname']);
            $dob = date_create_from_format('Y-m-d', $data['dob']);
            $response = Clog::get(sprintf(
                '/v2/clients/type/1/surname/%s/birth_day/%s/birth_month/%s/birth_year/%s/field/id;first_name;surname;email;postcode',
                $surname,
                $dob->format('j'),
                $dob->format('n'),
                $dob->format('Y')
            ));
            $errors = self::handle_api_errors($response);
            if (!$errors) {
                if (empty($response['body'])) {
                    // new client
                    return null;
                } else {
                    foreach ($response['body'] as $body) {

                        if (isset($body['email']) && strtolower(trim($body['email'])) === strtolower(trim($data['email']))) {
                            return $body['id'];
                        }
                        if (isset($body['postcode'])) {
                            $trimmed_body_postcode = str_replace(" ", "", strtoupper($body['postcode']));
                            $trimmed_data_postcode = str_replace(" ", "", strtoupper($data['postcode']));
                            if ($trimmed_body_postcode === $trimmed_data_postcode) {
                                return $body['id'];
                            }
                        }
                        if (isset($body['first_name'])) {
                            $trimmed_body_fname = str_replace(" ", "", strtoupper($body['first_name']));
                            $trimmed_data_fname = str_replace(" ", "", strtoupper($data['first_name']));
                            if ($trimmed_body_fname === $trimmed_data_fname) {
                                return $body['id'];
                            }
                        }
                    }
                    // couldn't match on any details, so create new record?
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * Send search request to CLOG, searching by surname and DOB
     */
    public static function search_dependant($data, $client)
    {
        if (!$client) {
            return false;
        }
        if (isset($data['dependant_first_name']) && isset($data['dependant_surname']) && isset($data['client'])) {
            $response = Clog::get(sprintf(
                '/v2/clients/id/%s/field/relationship_details',
                $client
            ));
            $errors = self::handle_api_errors($response);
            if (!$errors) {
                if (empty($response['body'])) {
                    // new client
                    return null;
                } else {
                    foreach ($response['body'] as $body) {
                        if (isset($body['relationship_details'])) {
                            foreach ($body['relationship_details'] as $dependant_id => $relationship) {
                                $response = Clog::get(sprintf(
                                    '/v2/clients/id/%s/field/first_name;surname',
                                    $dependant_id
                                ));
                                $errors = self::handle_api_errors($response);
                                if (!$errors) {
                                    if (!empty($response['body'])) {
                                        foreach ($response['body'] as $body) {
                                            if (isset($body['first_name']) && isset($body['surname'])) {
                                                $trimmed_body_fname = str_replace(" ", "", strtoupper($body['first_name']));
                                                $trimmed_data_fname = str_replace(" ", "", strtoupper($data['dependant_first_name']));
                                                if ($trimmed_body_fname === $trimmed_data_fname) {
                                                    $trimmed_body_surname = str_replace(" ", "", strtoupper($body['surname']));
                                                    $trimmed_data_surname = str_replace(" ", "", strtoupper($data['dependant_surname']));
                                                    if ($trimmed_body_surname === $trimmed_data_surname) {
                                                        return $dependant_id;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // couldn't match on any details, so create new dependant?
                    return false;
                }
            }
        }
        return false;
    }

    public static function is_under_18($dob) {
        $now_minus_18 = strtotime("-18 years");
        $dob = strtotime($dob . ' 00:00:00');
        if ($now_minus_18 > $dob) {
            return false;
        }
        return true;
    }

    public static function test_api_dictionary(\WP_REST_Request $request)
    {
        // comment below line to test
        return null;
        $response = Clog::get('/v2/dictionary');
        return $response;
    }

    public static function save_submission(\WP_REST_Request $request)
    {
        if (!wp_verify_nonce($request['action_nonce'], 'gp_referrals_action_nonce_referring')) {
            return new \WP_Error('auth', 'Invalid request', ['status' => 400]);
        }
        $data = $request->get_params();
        $type = $request->get_param('form_type');
        unset($data['form_type']);
        unset($data['action_nonce']);

        $data = self::handle_data($data, $type);

        $errors = Validation::validate_request($data, $type, $request);
        if ($errors) {
            return new \WP_Error('auth', $errors, ['status' => 400]);
        }
        // honeypot to fool bots
        unset($data['request_key']);

        // if updating a previous submission
        if (isset($data['submission_post_id']) && isset($data['submission_hash'])) {
            if ($type != 'further-support' && $type != 'new-to-caring') { // if not part 2 of new-to-caring
                return new \WP_Error('auth', 'Bad request', ['status' => 400]);
            }
            $submission = Submission::get($data['submission_post_id']);
            if ($submission['data']['submission_hash'] !== $data['submission_hash']) {
                return new \WP_Error('auth', 'Bad request', ['status' => 400]);
            }
            // if we saved email, validate against it
            if (
                isset($submission['data']['email']) && isset($data['email']) && $data['email'] !== $submission['data']['email']
            ) {
                return new \WP_Error('auth', 'Bad request', ['status' => 400]);
            }
            unset($data['submission_hash']);
            unset($data['submission_post_id']);
        }
        if ($type == 'new-to-caring') { // we need to get resource articles
            $data['resources'] = Resources::calculate_results($data);
        }
        if ($type == 'further-support') {
            $type == 'new-to-caring';
        }
        if (isset($submission)) {
            $save = Submission::save_fields($submission['id'], $data);
            $response = array('submission_post_id' => $save['submission_post_id'], 'submission_hash' => $submission['data']['submission_hash']);
        } else {
            $save = Submission::save($data, $type);
            $response = array('submission_post_id' => $save['submission_post_id'], 'submission_hash' => $save['submission_hash']);
        }
        if (isset($data['resources'])) {
            $response['referral_resources'] = $data['resources'];
        }

        do_action('gp_referrals_after_save_submission', $data, $response, $type);


        $submission = Submission::get($save['submission_post_id']);
        self::handle_clog_requests($submission, 'submit');

        return $response;
    }

    public static function handle_clog_requests_from_thank_you($submission_post_id, $submission_hash)
    {
        $submission = Submission::get($submission_post_id);
        if ($submission && $submission_hash === $submission['data']['submission_hash']) {
            self::handle_clog_requests($submission, 'thank-you');
        }
        return false;
    }

    public static function handle_clog_requests_from_admin($submission_post_id)
    {
        $submission = Submission::get($submission_post_id);
        if ($submission) {
            self::handle_clog_requests($submission, 'admin');
        }
        return false;
    }

    public static function handle_clog_requests($submission, $from)
    {

        $submission_post_id = $submission['id'];

        $data = $submission['data'];

        if (isset($data['register']) && $data['register'] != 'true') {
            return;
        }

        switch ($submission['type']) {
            case 'third-party':
            case 'self-registration':
                if (isset($submission['data']['sent_to_clog']) && $submission['data']['sent_to_clog'] === 'yes') {
                    return;
                }
                break;
                // if submitting page 2 of the form, we may have already sent page 1, in which case we should only send triage referral
        }

        $data['referral_sent'] = self::convert_to_boolean('referral_sent', $data);
        $data['triage_referral_sent'] = self::convert_to_boolean('triage_referral_sent', $data);
        $data['support_required'] = self::convert_to_boolean('support_required', $data);

        if ($submission['type'] == 'new-to-caring' || $submission['type'] == 'further-support') {
            // if no support required and we have already sent referral, skip
            if (!$data['support_required'] && $data['referral_sent']) {
                return;
            }
            // if support required and we have already sent support referral, skip
            if ($data['support_required'] && $data['triage_referral_sent']) {
                return;
            }
        }

        // get the array of requests we need to run
        $clog_requests = ClogRequests::get($submission['type']);

        if (!$clog_requests) {
            return;
        }

        // get whether new or existing carer
        $data['client'] = isset($data['client']) ? $data['client'] : self::search_client($data);
        $data['new_client'] = $data['client'] ? false : true;
        $data['client_under_18'] = self::is_under_18($data['dob']);
        if ($submission['type'] == 'new-to-caring' || $submission['type'] == 'further-support') {
            $data['dependant'] = self::search_dependant($data, $data['client']);
            $data['new_dependant'] = $data['dependant'] ? false : true;
        }

        // $support_required = $data['support_required'];
        // $dependant = $data['dependant'];
        // $new_dependant = $data['new_dependant'];

        foreach ($clog_requests as $identifier => $api_request) {
            if (Requests::should_run($data, $api_request)) {
                $run = Requests::prepare_request($data, $api_request, $submission);
                if (!empty($run['data'])) { // might be all conditional fields
                    $formatted_query = str_replace(array("Array", "(", ")"), "", print_r($run, true));
                    Log::create(
                        array(
                            'submission_post_id' => $submission_post_id,
                            'status' => Log::STATUS_NOTICE,
                            'action' => 'sending_clog_request',
                            'message' => $formatted_query
                        )
                    );
                    $response = Clog::send($run['method'], $run['endpoint'], $run['data'], $run['headers']);
                    $formatted_response = str_replace(array("Array", "(", ")"), "", print_r(array($response), true));
                    Log::create(
                        array(
                            'submission_post_id' => $submission_post_id,
                            // Is response code 2xx ? eg. 200/201?
                            'status' => substr($response['code'], 0, 1) == '2' ? Log::STATUS_NOTICE : Log::STATUS_ERROR,
                            'action' => 'clog_response',
                            'message' => $formatted_response
                        )
                    );
                    if (substr($response['code'], 0, 1) !== '2') { // if error
                        if ($from !== 'submit') { // not submit to prevent duplicate email
                            self::send_error_email($submission_post_id, $formatted_response);
                        }
                        Submission::save_fields($submission_post_id, array('client' => $data['client'], 'sent_to_clog' => 'error'));
                        return $data;
                    }
                    $data = Requests::handle_response($data, $response, $api_request);
                }
            }
        }
        $update = array(
            'client' => $data['client'],
            'sent_to_clog' => 'yes',
        );
        if ($submission['type'] == 'new-to-caring' || $submission['type'] == 'further-support') {
            $update['referral_sent'] = $data['referral_sent'];
            $update['triage_referral_sent'] = $data['triage_referral_sent'];
            $update['dependant'] = $data['dependant'];
        }
        Submission::save_fields($submission_post_id, $update);
        return $data;
    }

    public static function send_error_email($submission_post_id, $formatted_response)
    {
        if (!apply_filters('gp_referrals_send_error_email', true)) {
            return;
        }

        $subject = sprintf('Error with %s Referral Submission', get_bloginfo('name'));
        $to = 'sarah@giantpeach.agency';
        $headers[] = 'From: Carer Support Wiltshire <noreply@carersupportwiltshire.co.uk>';

        if (\WP_ENV === 'production') {
            $to = 'andrews@carersupportwiltshire.co.uk';
            $headers[] = 'Cc: support@giantpeach.agency';
        }

        $body = sprintf('This email is being sent because an error occurred with a Referral Submission (ID #%s) on %s. This will normally be an error returned from the CLOG API. The raw error is below: ' . "\n\n", $submission_post_id, get_bloginfo('name'));
        $body .= $formatted_response . "\n\n";
        $body .= sprintf('To review the submission, please log in at %s where you can view the full log.', get_site_url(null, '/wp-admin'));

        $headers = apply_filters('gp_referrals_error_email_headers', $headers);
        $to = apply_filters('gp_referrals_error_email_to', $to);
        $body = apply_filters('gp_referrals_error_email_body', $body);
        $subject = apply_filters('gp_referrals_error_email_subject', $subject);

        wp_mail($to, $subject, $body, $headers);
    }
}

Api::init();
