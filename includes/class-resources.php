<?php

namespace GP_Referrals;

defined('ABSPATH') || exit;

class Resources
{
    /**
     * Hook in methods.
     */
    public static function init()
    {
        add_shortcode('referral_resources', array(__CLASS__, 'render_resources'), 10, 3);
        add_filter('gp_referral_resources_list', array(__CLASS__, 'filter_resources_list'), 10, 1);
    }

    public static function calculate_results($data)
    {
        $matrix = self::get_matrix();
        $resources = array();
        foreach ($matrix as $field_name => $matches) {
            if (isset($data[$field_name])) {
                $value = $data[$field_name];
                foreach ($matches as $possible_value => $ids) {
                    if (is_array($value)) {
                        if (in_array($possible_value, $value)) {
                            $resources = array_merge($resources, $ids);
                        }
                    } else if ($possible_value == $value) {
                        $resources = array_merge($resources, $ids);
                    }
                }
                // Wildcard
                if (isset($matches['*'])) {
                    $resources = array_merge($resources, $matches['*']);
                }
            }
        }
        // Sort values by most common matches to least common matches
        $occurences = array_count_values($resources);
        arsort($occurences);

        return array_keys($occurences);
    }

    public static function get_matrix()
    {
        $matrix = array(
            'relationship' => array(
                // My child
                '7;5' => array(963),
                // My parent
                '5;7' => array(6057),
                // Spouse/partner
                '4;4' => array(6057),
                // Brother or sister
                '9:9' => array(6057),
                // Friend/neighbour
                '10;10' => array(6057)
            ),
            'why_questionnaire' => array(
                'my_caring_situation_is_becoming_more_complex' => array(6213),
                '_relative_first_name_parent_type_ie_mum_dad_is' => array(6213),
                'i_think_relative_name_parent_is_reaching_the' => array(6213),
                'i_rsquo_m_giving_care_already_but_i_need_support' => array(6213),
                'i_rsquo_m_just_thinking_ahead' => array(6213)
            ),
            'caring_responsibilities' => array(
                'personal_care_such_as_washing_dressing' => array(6202), // Equipment & tech
                'help_with_moving_around' => array(6202),
                'practical_help_such_as_cooking_meals_doing_the' => array(6202),
                'arranging_medical_appointments_amp_care_services' => array(6202),
                'giving_medicines' => array(6202),
                'helping_with_paperwork' => array(6242),
                'keeping_an_eye_on_them_to_make_sure_they_are_safe' => array(6202),
                'providing_emotional_support' => array(969), // Mental Health
                'advocating_for_them_speaking_on_their_behalf_' => array(6215), // Advocacy
            ),
            'taken_ill_be_ok' => array(
                '1630' => array(1592),
                '1631' => array(1592),
                '1632' => array(1592),
            ),
            'are_you_able_to_leave_alone' => array(
                '1634' => array(6202),
                '1635' => array(6202),
            ),
            'main_disability' => array(
                // Alzheimers
                '1' => array(968),
                // Parkinsons
                '24' => array(960),
                // Mental health
                '19' => array(969),
                // Stroke
                '30' => array(959),
                // Substance misuse
                '31' => array(959),
                // All other conditions
                '*' => array(6201),
            ),
            'work_status' => array(
                // Alzheimers
                'F' => array(974),
                // Parkinsons
                'P' => array(974),
                // Looking for work
                'L' => array(974),
                // Care full time
                'C' => array(971),
            ),
            'how_would_you_describe_your_financial_situation_' => array(
                // Doing ok
                '1636' => array(971),
                // Tight
                '1637' => array(971),
                '1638' => array(971),
                '1639' => array(971),
                '1640' => array(971),
            ),
            'does_your_caring_role_still_allow_you_time_for' => array(
                // Difficult to spend time away
                '1642' => array(964),
                // Impossible to spend
                '1643' => array(964)
            ),
            'how_do_you_feel_about_your_caring_role_' => array(
                // Worried taking over life
                '1647' => array(6213),
                // Not much life outside caring
                '1637' => array(6213),
                // Taken over life
                '1638' => array(6213),
            ),
            'help_manage_role' => array(
                'regular_breaks_from_my_caring_role' => array(964),
                'i_would_like_training_in_a_specific_area_e_g_' => array(973),
                'being_able_to_talk_about_my_caring_role' => array(1603),
            )
        );

        return apply_filters('gp_referrals_resource_matrix', $matrix);
    }

    /**
     * Get the resources results to display from a saved cookie
     */
    public static function render_resources()
    {
        $found = false;
        ob_start();
?>
        <div class="row">
            <?php
            if (isset($_COOKIE['gp_referral_resources'])) {
                $resources = explode(",", $_COOKIE['gp_referral_resources']);
                if (!empty($resources)) {

                    if (\WP_ENV !== 'production') {
                        echo "<p class='col-md-12'>[For Testing Only] Resources Matched from Answers: " . implode(", ", $resources) . "</p>";
                    }

                    $resources = apply_filters('gp_referral_resources_list', $resources);
                    $resources_limit = apply_filters('gp_referral_resources_limit', 6);
                    $resources_counter = 0;
                    foreach ($resources as $resource) {

                        $post_object = \get_post($resource);
                        if (is_object($post_object) && $post_object->post_type === 'resource-centre') {
                            \setup_postdata($GLOBALS['post'] = &$post_object);
                            $resources_counter++;
                            // first post
                            if (!$found) {
                                $found = true;
            ?>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <p style="font-size: 20px"><strong>Based on your answers to our questions, the following resources might be helpful to you:</strong></p>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                            <div class="col-sm-6 col-md-4">
                                <div href="<?php echo \get_permalink(); ?>" class="resource-block has-thumb resource-block--page">
                                    <div class="resource-block__thumb">
                                        <picture>
                                            <?php echo \get_resource_thumbnail_image_src(null, null, null); ?>
                                        </picture>
                                    </div>
                                    <div class="resource-block__content">
                                        <div class="resource-block__header">
                                            <h2 class="h5"><?php the_title(); ?></h2>
                                        </div>
                                        <div class="resource-block__excerpt">
                                            <?php echo \resource_content_excerpt($resource); ?>
                                        </div>
                                        <a href="<?php echo \get_permalink(); ?>" class="btn btn--transparent">Read Article</a>
                                    </div>
                                </div>
                            </div>
                <?php
                            if ($resources_counter >= $resources_limit) {
                                break;
                            }
                        }
                    }
                }
            }
            wp_reset_postdata();
            if (!$found) {
                ?>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-10">
                            <p>Thanks for taking our questionnaire. We didn't find any specific results to match your answers, but our <a href="/resource-centre">Resource Centre</a> might be helpful to you.</p>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>
        </div>
<?php
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
    }

    public static function filter_resources_list($resources)
    {
        if (\WP_ENV !== 'production') {
            // replace live resource ids with dev resource ids
            $replacements = array(
                6057 => 2562,
                6202 => 2561,
                6215 => 2560
            );
            foreach ($resources as $key => $value) {
                foreach ($replacements as $search => $replace) {
                    if ((int) $value === (int) $search) {
                        $resources[$key] = $replace;
                    }
                }
            }
            return $resources;
        }
        return $resources;
    }
}
Resources::init();
