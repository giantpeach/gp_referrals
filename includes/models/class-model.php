<?php

namespace GP_Referrals;

defined( 'ABSPATH' ) || exit;
class Model {

    protected static $table_name_unprefixed = 'gp_referrals_exam_sessions';

    protected static $encrypted_fields = array();
    protected static $json_fields = array();

    public function __construct( $data = array() ) {
        $this->set_data($data);
    }

    public static function get_table_name() {
        global $wpdb;
        return $wpdb->prefix . static::$table_name_unprefixed;
    }

    public static function load( $id ) {
        global $wpdb;
        $record = $wpdb->get_row(
            "SELECT * FROM `" . static::get_table_name() . "` WHERE `id`={$id}",
            ARRAY_A
        );
        if ($record) {
            $object = new static(static::decode_values($record));
            return $object;
        }
        else {
            return false;
        }
    }

    public static function create( $data ) {

        $object = new static($data);
        $id = $object->insert();

        if ($id) {
            $object->set('id', $id);
            return $object;
        }

        return false;
    }

    public static function load_where( $where = array() ) {
        global $wpdb;

        $sql = "SELECT * FROM `" . static::get_table_name() . "` WHERE 1=1";

        foreach ($where as $column_name => $value) {
         $sql .= " AND `{$column_name}`='{$value}'";
        }

        $record = $wpdb->get_row(
            $sql,
            ARRAY_A
        );

        if ($record) {
            $object = new static(static::decode_values($record));
            return $object;
        }
        else {
            return false;
        }

    }

    // public function create( $data ) {
    //     global $wpdb;
    //     $this->set_data($data);
    //     $id = $this->insert();
    //     $this->set('id', $id);
    // }

    public function delete() {
        global $wpdb;
        $record = $wpdb->delete(
            $this->get_table_name(),
            ['id' => $this->get('id')],
            [ '%d' ]
        );
        return $record;
    }

    public function insert() {
        global $wpdb;
        $data = static::encode_values($this->get_data());
        $record = $wpdb->insert(
            $this->get_table_name(), $data
        );
        return $wpdb->insert_id;
    }

    public function save() {
        global $wpdb;
        $data = static::encode_values($this->get_data());
        $record = $wpdb->update(
            $this->get_table_name(), $data, [ 'id' => $this->get('id') ]
        );
        return $record;
    }

    public function set_data( $data ) {
        $this->data = array_merge($this->get_data(), $data);
    }

    public function set( $key, $value ) {
        $this->data[$key] = $value;
    }

    public function get_data( ) {
        return $this->data;
    }

    public function get( $key ) {
        return $this->data[$key];
    }

    /**
     * encrypt the fields which should be encrypted
     * and json_encode the fields which should be json encoded
     */
    public static function encode_values( $data ) {
        $encoded_data = array();
        foreach ($data as $key => $value) {
            if (in_array($key, static::$encrypted_fields)) {
                $encoded_data[$key] = is_null($value) ? null : static::encrypt($value);
            }
            elseif (in_array($key, static::$json_fields)) {
                $encoded_data[$key] = is_null($value) ? null : json_encode($value);
            }
            else {
                $encoded_data[$key] = $value;
            }
        }
        return $encoded_data;
    }

    /**
     * decrypt the fields which should be encrypted
     * and json_decode the fields which should be json encoded
     */
    public static function decode_values( $data ) {
        $decoded_data = array();
        foreach ($data as $key => $value) {
            if (in_array($key, static::$encrypted_fields)) {
                $decoded_data[$key] = is_null($value) ? null : static::decrypt($value);
            }
            elseif (in_array($key, static::$json_fields)) {
                $decoded_data[$key] = is_null($value) ? null : json_decode($value);
            }
            else {
                $decoded_data[$key] = $value;
            }
        }
        return $decoded_data;
    }

    public static function encrypt( $value ) {
        $iv = openssl_random_pseudo_bytes( openssl_cipher_iv_length( 'aes-256-ctr' ) );
        $value = openssl_encrypt( $value, 'aes-256-ctr', AUTH_KEY, 0, $iv );
        $value = bin2hex( $iv ) . $value;

        return $value;
    }

    public static function decrypt( $value ) {

        $iv_size = openssl_cipher_iv_length( 'aes-256-ctr' );
		$iv_size = 32;
		$iv = hex2bin(substr( $value, 0, $iv_size ));
		$value = openssl_decrypt( substr( $value, $iv_size ), 'aes-256-ctr', AUTH_KEY, 0, $iv );

        return $value;
    }

}
