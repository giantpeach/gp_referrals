<?php

namespace GP_Referrals;

defined('ABSPATH') || exit;

class Log extends Model
{

    const STATUS_ERROR = 0;
    const STATUS_RESOLVED = 1;
    const STATUS_NOTICE = 2;

    protected static $table_name_unprefixed = 'gp_referrals_logs';

    protected $data = array(
        'id'                => null,
        'submission_post_id'   => null,
        'action'            => null,
        'message'            => null,
        'created_at'        => null,
        'status'            => 0,
    );

    protected static $encrypted_fields = array('message');

    public static function create($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s');
        return parent::create($data);
    }
}
