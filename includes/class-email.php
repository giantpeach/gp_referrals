<?php

namespace GP_Referrals;

defined('ABSPATH') || exit;

class Email
{
    /**
     * Hook in methods.
     */
    public static function init()
    {
        add_action('gp_referrals_after_save_submission', array(__CLASS__, 'send_results_email'), 10, 3);
    }

    public static function set_to_html()
    {
        return 'text/html';
    }

    public static function send_results_email($data, $response, $type)
    {
        if ($type != 'new-to-caring') {
            return;
        }
        if (!isset($data['email']) || empty($response['referral_resources'])) {
            return;
        }

        if (!isset($data['results_opt_in']) || $data['results_opt_in'] !== 'true') {
            return;
        }

        $subject = 'Your results from Carer Support Wiltshire';
        $body = self::email_template($data, $response['referral_resources']);
        if (empty($body)) {
            return;
        }
        add_filter('wp_mail_content_type', array(__CLASS__, 'set_to_html'));
        wp_mail($data['email'], $subject, $body);
        remove_filter('wp_mail_content_type', array(__CLASS__, 'set_to_html'));
    }

    public static function email_template($data, $resources)
    {
        $resources = \apply_filters('gp_referral_resources_list', $resources);
        if (empty($resources)) {
            return;
        }
        \ob_start();
        $logo_src = get_home_url() . '/app/assets/images/carer-support-wiltshire-logo.png';
        $logo_src = str_replace('http://www.csw.loc:8888/', 'http://carersupportwiltshire.nb8.giantpeachtest.com/', $logo_src);
?>
        <div style="width: 100%; -webkit-text-size-adjust: none !important; margin: 0; padding: 30px 0 50px 0; background-color: #f7fcfe;">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td align="center" valign="top">
                            <div id="template_header_image">
                                <p style="margin-top: 0;"><img class="" src="<?php echo $logo_src; ?>" alt="" width="300" /></p>

                            </div>
                            <table id="template_container" style="box-shadow: 0 0 0 1px #f3f3f3 !important; border-radius: 3px !important; background-color: #ffffff; border: 1px solid #e9e9e9; padding: 0 0 020px;" border="0" width="600" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td align="center" valign="top">
                                            <table id="template_body" border="0" width="600" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td style="border-radius: 3px !important; font-family: 'Poppins', sans-serif;" valign="top">
                                                            <table border="0" width="100%" cellspacing="0" cellpadding="20">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <div style="color: #000000; font-size: 14px; font-family: 'Poppins', sans-serif; line-height: 150%; text-align: left;">
                                                                                <p class="p1" style="font-size: 26px; line-height: 36px; color: #1f2041; text-align: center;"><span class="s1">Thank you for taking the <b>New to caring</b> questionnaire on our website.</span></p>
                                                                                <p class="p3" style="font-size: 22px; line-height: 28px; color: #1f2041; text-align: center;"><span class="s1">Based on your answers, the resources below may help you with your caring role.</span></p>
                                                                                <p class="p4" style="font-size: 18px; text-align: center;"><span class="s1">If you need any more information or support, please visit our website: <a style="text-decoration: none; color: #2cab6c;" href="https://carersupportwiltshire.co.uk/">carersupportwiltshire.co.uk</a> or call 0800 181 4118. We are here to help.</span></p>
                                                                                <table border="0" width="100%" cellspacing="0" cellpadding="20">
                                                                                    <?php
                                                                                    $count = 0;
                                                                                    foreach ($resources as $resource) :
                                                                                        $post_type = \get_post_type($resource);
                                                                                        if ($post_type == 'resource-centre') :

                                                                                            $title = \get_the_title($resource);
                                                                                            $link = \get_the_permalink($resource);
                                                                                            $type = \get_field('resource_type', $resource);
                                                                                            $image = \get_field('resource_thumbnail', $resource);
                                                                                            $title = \get_the_title($resource);
                                                                                            $src = false;
                                                                                            if ($image) {
                                                                                                $src = \wp_get_attachment_image_src($image['id'], 'square-thumb')[0];
                                                                                            }
                                                                                            if (!$src) {
                                                                                                if ($type == 'video') {
                                                                                                    $src = \get_home_url() . '/app/assets/images/resource-placeholder-video.png';
                                                                                                } elseif ($type == 'file') {
                                                                                                    $src = \get_home_url() . '/app/assets/images/resource-placeholder-file.png';
                                                                                                } elseif ($type == 'url') {
                                                                                                    $src = \get_home_url() . '/app/assets/images/resource-placeholder-url.png';
                                                                                                } else {
                                                                                                    $src = \get_home_url() . '/app/assets/images/resource-placeholder-page.png';
                                                                                                }
                                                                                            }
                                                                                            if ($src) : $count++;
                                                                                                if ($count % 2 === 1) echo "<tr>";

                                                                                                $src = str_replace('http://www.csw.loc:8888/', 'http://carersupportwiltshire.nb8.giantpeachtest.com/', $src);

                                                                                    ?>
                                                                                                <td width="50%" valign="top">
                                                                                                    <table border="0" width="280" cellspacing="0" cellpadding="0">
                                                                                                        <tr>
                                                                                                            <td style="border-radius: 3px !important; font-family: 'Poppins', sans-serif;" valign="top">
                                                                                                                <a href="<?php echo $link; ?>"><img width="280" style="width: 100%" src="<?php echo $src; ?>" /></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td align="center" valign="middle" style="padding: 20px; border-radius: 3px !important; font-family: 'Poppins', sans-serif; background-color: #2cab6c; color: #fff; font-weight: 500;" valign="middle">
                                                                                                                <a style="color: #fff; font-size: 18px; text-decoration: none; text-align: center" href="<?php echo $link; ?>"><?php echo $title; ?></a>
                                                                                                            </td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                                </td>
                                                                                                <?php
                                                                                                $one = $count % 2;
                                                                                                $two = $count % 1;
                                                                                                if ($count % 2 === 0) echo "</tr>"; ?>
                                                                                            <?php endif; ?>
                                                                                        <?php endif; ?>
                                                                                    <?php endforeach; ?>
                                                                                </table>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <p class="p3" style="line-height: 22px; color: #1f2041; text-align: center; font-size: 18px; font-family: 'Poppins', sans-serif;"><span class="s1">Thank you,</span></p>
                                                                            <p class="p3" style="line-height: 22px; color: #1f2041; text-align: center; font-size: 18px; font-family: 'Poppins', sans-serif;"><span class="s1">Carer Support Wiltshire</span></p>
                                                                        </td>
                                                                    </tr>
                                                            </table>
        </div>
        </td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <table id="template_footer" style="border-top: 0; -webkit-border-radius: 3px;" border="0" width="600" cellspacing="0" cellpadding="10">
                    <tbody>
                        <tr>
                            <td valign="top"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
        </table>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
<?php
        $email_content = \ob_get_contents();
        \ob_end_clean();
        return $email_content;
    }
}
Email::init();
