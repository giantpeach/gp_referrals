<?php

namespace GP_Referrals;

defined('ABSPATH') || exit;

class Admin
{

    /**
     * Hook in methods.
     */
    public static function init()
    {
        add_action('add_meta_boxes', array(__CLASS__, 'add_referral_metabox'));
        add_filter('post_row_actions', array(__CLASS__, 'referral_row_actions'), 10, 1);
        add_action('admin_init', array(__CLASS__, 'hide_referral_title'), 10);
        add_action('admin_title', array(__CLASS__, 'referral_page_title'));
        add_filter('posts_join', array(__CLASS__, 'search_join'));
        add_filter('posts_where', array(__CLASS__, 'search_where'));
        add_filter('posts_distinct', array(__CLASS__, 'search_distinct'));
        add_filter('manage_gp_referral_posts_columns', array(__CLASS__, 'referral_posts_columns'), 10, 3);
        add_filter('manage_gp_referral_posts_custom_column', array(__CLASS__, 'referral_posts_custom_column'), 10, 3);
        // add_action('admin_head', array( __CLASS__, 'exam_module_id_column_width'));
        add_filter('bulk_actions-edit-gp_referral', array(__CLASS__, 'bulk_actions'));
        add_filter('handle_bulk_actions-edit-gp_referral', array(__CLASS__, 'clog_bulk_action_handler'), 10, 3);
        add_filter('admin_notices', array(__CLASS__, 'sent_to_clog_admin_notice'), 10);
    }

    public static function search_join($join)
    {
        global $pagenow, $wpdb;

        if (is_admin() && 'edit.php' === $pagenow && 'gp_referral' === $_GET['post_type'] && !empty($_GET['s'])) {
            $join .= 'LEFT JOIN ' . $wpdb->prefix . 'gp_referrals_entries AS referral_fields ON ' . $wpdb->posts . '.ID = referral_fields.entry_id';
        }
        return $join;
    }

    public static function search_where($where)
    {
        global $pagenow, $wpdb;

        if (is_admin() && 'edit.php' === $pagenow && 'gp_referral' === $_GET['post_type'] && !empty($_GET['s'])) {

            $hashed_search = urldecode($_GET['s']);
            $hashed_search = strtolower($hashed_search);
            $hashed_search = hash('sha256', env('AUTH_KEY') . $hashed_search);
            $where = preg_replace(
                "/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                "(referral_fields.value = '" . $hashed_search . "')",
                $where
            );
        }
        return $where;
    }

    public static function search_distinct($where)
    {
        global $pagenow, $wpdb;

        if (is_admin() && 'edit.php' === $pagenow && 'gp_referral' === $_GET['post_type'] && !empty($_GET['s'])) {
            return "DISTINCT";
        }
        return $where;
    }


    public static function add_referral_metabox()
    {
        add_meta_box('gp_referral', 'Referral', array(__CLASS__, 'gp_referral_metabox'), 'gp_referral', 'normal', 'high');
        add_meta_box('gp_referral_logs', 'Logs', array(__CLASS__, 'gp_referral_logs'), 'gp_referral', 'normal');
    }

    public static function referral_row_actions($actions)
    {
        if (get_post_type() === 'gp_referral') {
            if (isset($actions['edit'])) {
                $actions['edit'] = str_replace("Edit", "View", $actions['edit']);
            }
            unset($actions['inline hide-if-no-js']);
        }
        return $actions;
    }

    public static function hide_referral_title()
    {
        remove_post_type_support('gp_referral', 'title');
    }

    public static function referral_page_title()
    {
        global $post, $title, $action, $current_screen;
        if (isset($current_screen->post_type) && $current_screen->post_type == 'gp_referral' && $action == 'edit') {
            /* this is the new page title */
            $title = 'Referral #' . $post->ID;
        }
        return $title;
    }

    public static function gp_referral_metabox()
    {
        global $post;
        $submission = Submission::get($post->ID);
        $json = Fields::json($submission['type']);
        if (!$json) {
            echo "<p>An error occured displaying the referral.</p>";
            return;
        }
        // $fields = Fields::all($submission['type']);

        foreach ($json['pages'] as $page) {
            // printf("<h3>%s</h3>", $page['title']);
            foreach ($page['fieldsets'] as $fieldset) {
                $html = "";
                foreach ($fieldset['fields'] as $field) {
                    if ($field['name'] === 'support_required') {
                        $catch = true;
                    }
                    if (isset($submission['data'][$field['name']])) {
                        $value = $submission['data'][$field['name']];
                        $html .= sprintf("<strong>%s</strong><br>", Fields::get_field_label($field['name'], $fieldset['fields']));
                        $html .= sprintf("%s<br>", Fields::get_field_value($value, $field, 'ul'));
                        $html .= "<br>";
                        unset($submission['data'][$field['name']]);
                    }
                }
                // Commenting as it looks weird to display title without fields
                if (isset($fieldset['title']) && !empty($html)) {
                    printf("<h3>%s</h3>", $fieldset['title']);
                }
                echo $html;
                if (!empty($html)) {
                    printf("<hr>");
                }
            }
        }

        /* Fields we saved that are no longer in the fieldset JSON */
        foreach ($submission['data'] as $field => $value) {
            if ($field == 'submission_hash') {
                continue;
            }
            printf("<strong>%s</strong><br>", $field);
            if (is_array($value)) {
                printf("%s<br>", implode(", ", $value));
            } else {
                printf("%s<br>", $value);
            }
            echo "<br>";
        }
    }

    public static function gp_referral_logs()
    {
        global $post;
        $logs = Submission::get_logs($post->ID);
        // $fields = Fields::all($submission['type']);

        if (count($logs)) : ?>
            <style>
                .reported-problem {
                    background: #fff;
                    border: 1px solid #ccd0d4;
                    border-left: 4px solid #dc3232;
                    box-shadow: 0 1px 1px rgba(0, 0, 0, .04);
                    margin: 5px 0 1rem;
                    padding: 1px 12px;
                }

                .reported-problem.resolved {
                    border-left-color: #46b450;
                }

                .reported-problem.log {
                    border-left-color: #888;
                }

                .reported-problem a {
                    text-decoration: none;
                }
            </style>
            <p>The below notices have been reported/logged for this submission.</p>
            <?php foreach ($logs as $log) {

                switch ((int)$log['status']) {
                    case Log::STATUS_NOTICE:
                        $class = 'log';
                        $status_label = __('Log');
                        break;
                    case Log::STATUS_RESOLVED:
                        $class = 'resolved';
                        $status_label = __('Resolved Error');
                        break;
                    default:
                        $class = '';
                        $status_label = __('Error');
                }

                switch ($log['action']) {
                    case 'submission_saved':
                        $action_label = 'Submission saved.';
                        break;
                    case 'clog_response':
                        $action_label = 'Response from CLOG Api.';
                        break;
                    case 'sending_clog_request':
                        $action_label = 'Sending data to CLOG Api';
                        break;
                    default:
                        $action_label = $log['action'];
                }

            ?>
                <div class="reported-problem <?php echo $class; ?>">
                    <p>
                        <strong>Status:</strong> <?php echo $status_label ?><br />
                        <strong>Created At:</strong> <?php echo $log['created_at']; ?> (#<?php echo $log['id']; ?>) <br />
                        <?php if ($log['action']) : ?>
                            <strong>Action:</strong> <?php echo $action_label; ?><br />
                        <?php endif; ?>
                    </p>
                    <?php if ($log['message']) : ?>
                        <label><strong>Message</strong></label>
                        <p><?php echo nl2br(Log::decrypt($log['message'])) ?></p>
                    <?php endif; ?>
                </div>
<?php
            }
        endif;
    }

    public static function referral_posts_columns($columns)
    {
        unset($columns['title']);
        unset($columns['date']);
        $columns['id'] = __('ID', 'gp_referrals');
        $columns['first_name'] = __('First name', 'gp_referrals');
        $columns['surname'] = __('Surname', 'gp_referrals');
        $columns['email'] = __('Email');
        $columns['sent_to_clog'] = __('Sent to Clog?');
        $columns['client'] = __('CLOG ID');
        $columns['errors'] = __('Errors?');
        $columns['date'] = __('Date');
        return $columns;
    }

    public static function referral_posts_custom_column($column, $post_id)
    {
        $submission = Submission::get($post_id);
        switch ($column) {
            case 'first_name':
            case 'surname':
            case 'email':
            case 'client':
                echo isset($submission['data'][$column]) ? $submission['data'][$column] : null;
                break;
            case 'sent_to_clog':
                if (isset($submission['data']['register']) && $submission['data']['register'] === 'true') {
                    echo isset($submission['data'][$column]) ? $submission['data'][$column] : 'Not sent';
                } else {
                    echo "n/a";
                }
                break;
            case 'errors':
                $errors = Submission::get_logs($post_id, array(Log::STATUS_ERROR));
                if (empty($errors)) {
                    echo 'No';
                } else {
                    echo "<span style='color: red'>Yes</span>";
                }
                break;
            case 'id':
                echo $post_id;
        }
    }

    public static function bulk_actions($bulk_actions)
    {
        $bulk_actions['send_to_clog'] = __('Send to CLOG', 'gp_referral');
        $bulk_actions['download_csv'] = __('Download CSV', 'gp_referral');
        return $bulk_actions;
    }

    public static function clog_bulk_action_handler($redirect_to, $doaction, $post_ids)
    {
        if ($doaction !== 'send_to_clog') {
            return $redirect_to;
        }
        foreach ($post_ids as $post_id) {
            do_action('send_to_clog', $post_id);
        }
        $redirect_to = add_query_arg('bulk_sent_to_clog_posts', count($post_ids), $redirect_to);
        return $redirect_to;
    }
    public static function download_csv_bulk_action_handler($redirect_to, $doaction, $post_ids)
    {
        if ($doaction !== 'download_csv') {
            return $redirect_to;
        }
        $rows = array();
        foreach ($post_ids as $post_id) {
            $submission = Submission::get($post_id);
            if (!empty($submission['data'])) {
                $rows[] = $submission['data'];
            }
        }
        $redirect_to = add_query_arg('bulk_sent_to_clog_posts', count($post_ids), $redirect_to);
        return $redirect_to;
    }

    public static function sent_to_clog_admin_notice()
    {
        if (!empty($_REQUEST['bulk_sent_to_clog_posts'])) {
            $emailed_count = intval($_REQUEST['bulk_sent_to_clog_posts']);
            printf('<div id="message" class="updated fade">' .
                _n(
                    'Sent %s submissions to CLOG.',
                    'Sent %s submissions to CLOG.',
                    $emailed_count,
                    'gp_referrals'
                ) . '</div>', $emailed_count);
        }
    }
}

Admin::init();
