<?php

namespace GP_Referrals;

defined('ABSPATH') || exit;

class Clog
{

    public static function get_host()
    {
        return env('CLOG_API_HOST');
    }

    public static function get_headers($custom_headers)
    {
        return array_merge(
            array(
                'Accept' => 'application/json',
                'Source' => env('CLOG_API_SOURCE_KEY'),
                'Org' => env('CLOG_API_ORG_KEY'),
                'User' => env('CLOG_API_USER_KEY'),
            ),
            $custom_headers
        );
    }

    public static function send($method, $endpoint, $data, $custom_headers = array())
    {
        $host = self::get_host();
        $headers = self::get_headers($custom_headers);

        $response = wp_remote_request($host . $endpoint, array(
            'method' => $method,
            'headers' => $headers,
            'body' => $data,
        ));

        if (is_wp_error($response)) {
            return false;
        }

        $response = array(
            'code' => $response['response']['code'],
            'message' => $response['response']['message'],
            'body' => $response['body'],
        );

        $response['body'] = json_decode($response['body'], true);

        return $response;
    }

    public static function put($endpoint, $data = array())
    {

        return self::send('PUT', $endpoint, $data);
    }

    public static function get($endpoint, $data = array())
    {

        return self::send('GET', $endpoint, $data);
    }

    public static function post($endpoint, $data = array())
    {

        return self::send('POST', $endpoint, $data);
    }
}
